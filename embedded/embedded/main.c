
#define F_CPU 3333333UL

#include <atmel_start.h>
#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "sensors.h"
#include "radio.h"

#define payloadSize 5



char data[40];
uint8_t i=0;


ISR(USART0_RXC_vect){				//Interrupt service routine for the receiver
	char data1 = USART0_RXDATAL;	//Read out the received data to a variable
	//char data2= USART0_RXDATAH;
	data[i++]=data1;
	//data[i++]=data2;
	if (data1=='\n' || i>39){
		i=0;
		reactOnRX(data);
	}
}


int main(void)
{
	atmel_start_init();
	uart_init(BAUD_57600);		//UART-LF
	//_delay_ms(10000);
	
	adc_init(); 				//Initiate the adc
	initSensors();
	sei();
	//setUartRateRadio(BAUD_9600); 	//wakes radio from sleep, but doesn't work yet, as well as sets the BAUD rate
	_delay_ms(2000);
	radioInit(); 				//initialize radio
	struct sensorData sd;		//Store data in struct
	char dataStr[payloadSize];	//Store data in char array
	
	
	while (1){
		//setUartRateRadio(BAUD_9600);
		readSensors(&sd);
		dataConv(&sd, dataStr);
		sendData(dataStr, payloadSize);
		//uart_print("system sleep 1000000\r\n");
		_delay_ms(15000);
	}
}
