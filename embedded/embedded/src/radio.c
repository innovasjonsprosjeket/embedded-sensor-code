#include "radio.h"

const char ASCII[]={0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46};


void uart_init(unsigned long baud){
	//From chapter 24.3 in datasheet
	PORTB.DIRSET = (1 << PIN2_bp); 	//Setting up TX pin as output
	PORTB.OUTSET = (1 << PIN2_bp); 	//Setting the TX pin high

	USART0.BAUDH = (baud >> 8); 		//Shift register right by 8 bits to get the 8 high bits
	USART0.BAUDL = baud; 				//Set baud rate without shifting to get the 8 low bits
	//It turns out the compiler can handle this automatically, meaning this works just as well:
	//USART0.BAUD = baud;

	//USART.CTRLC CMODE bits default to async, 1 stop bit, 8 bit character size

	USART0.CTRLB |= (1 << USART_RXEN_bp) | (1 << USART_TXEN_bp);	//Enable RX and TX
	USART0.CTRLA |= (1 << USART_RXCIE_bp); 							//Enable interrupts on incoming data
}


void uart_transmit(unsigned char data){					// function to transmit data
	while (!(USART0.STATUS & (1 << USART_DREIF_bp))){	// while(sending) -> wait
	};
	USART0.TXDATAL = data;						 		//Put our new data into se sending register
}


void uart_print(const char string[]){		//function to transmit character array
	uint16_t i=0;
	while (string[i]!='\0'){
		uart_transmit(string[i]);
		i++;
	}
}





void radioInit(void){
	PORTA.DIRSET = (1 << PIN7_bp);	//Set ~RST pin OUTPUT
	PORTA.OUTSET = (1 << PIN7_bp);	//Set ~RST pin high
	resetRadio();
}

void resetRadio(void){						//if radio doesn't behave
	uart_print("sys factoryRESET\r\n");		//TODO: reboot?

	// module: join The Things// Network (OTAA)
	_delay_ms(5000);
	uart_print("mac reset 868\r\n");
	_delay_ms(5000);
	uart_print("mac set appeui 70B3D57ED001698A\r\n");	
	_delay_ms(1000);
	uart_print("mac set deveui 006C1D34F6637EEA\r\n");
	//uart_print("mac get deveui\r\n");
	_delay_ms(1000);
	uart_print("mac set appkey 4CF67C5640B45FC84CD2F81A97DC15E9\r\n");
	_delay_ms(1000);
	uart_print("mac save\r\n");							//takes some time
	_delay_ms(5000);
	/*
	uart_print("mac get appeui\r\n");					//for debugging
	_delay_ms(1000);
	uart_print("mac get deveui\r\n");
	_delay_ms(1000);

	_delay_ms(100000);
	*/
	uart_print("mac join otaa\r\n");
	_delay_ms(15000);
}


void sendData(char sd[], uint8_t bytes){	// send data in char array format as string (hex)
  uart_print("mac tx uncnf 2 ");

  for (uint8_t i=0; i<bytes; i++){			//#bytes to send
		uart_transmit(ASCII[sd[i]/16]);		//convert uint16_t to byte
		uart_transmit(ASCII[sd[i]%16]);
  }
  uart_print("\r\n");
}

void dataConv(const struct sensorData* sd, char str[]){	//turns sensorData into char-array
	str[0]=sd->tds;
	str[1]=sd->turb;
	str[2]=10*sd->temp/256;
	str[3]=10*sd->temp%256;
	str[4]=sd->pH;
}

void sendError(char* hex){				//send errors on another port
	uart_print("mac tx uncnf 2 ");
	uart_print(hex);
	uart_print("\r\n");
}


void reactOnRX(char* hex){				//do something based on RX from radio module
	if (strncmp(hex, OK, 4)==0){
		return;
	}
	else if (strncmp(hex, INVALID, 15)==0){
		//radioInit();
	}
	else if (strncmp(hex, ACCEPTED, 10)==0){
		return;
	}
	else if (strncmp(hex, DENIED, 8)==0){
		resetRadio();
	}
	else if (strncmp(hex, NO_FREE_CH, 8)==0){
		//fyll p� stack
		//radioInit();
		resetRadio();
		_delay_ms(100000);
	}
	else if (strncmp(hex, TX_OK, 6)==0){
		//if noe i stack
		//send
		//fjern fra stack
		return;
	}
	else if(strncmp(hex, UNIN_KEYS, 15)==0){
		resetRadio();
	}
	else if(strncmp(hex, SILENT, 8)==0){
		return;
	}
	else if (strncmp(hex, BUSY, 6)==0){
		//add to stack?
		return;
	}
	else if (strncmp(hex, PAUSED, 8)==0){
		//start?
		return;
	}
	else if(strncmp(hex, NOT_JOINED, 10)==0){
		//radioInit();
		uart_print("mac join otaa\r\n");
		_delay_ms(20000);
	}
	else if(strncmp(hex, ERROR, 5)==0){
		resetRadio();
		_delay_ms(5000);
	}

	else{
		//sendError(hex);		//convert to ascii first	loop-> (char)hex[i]
	}
}


void setUartRateRadio(unsigned long baud){		//TODO: don't know, but not working as it is...
	//USART0.CTRLB &= ~((1 << USART_RXEN_bp) | (1 << USART_TXEN_bp));
	//PORTB.OUTTGL = (1 << PIN2_bp);
	//_delay_ms(1);
	//PORTB.OUTTGL = (1 << PIN2_bp);
	//USART0.CTRLB |= (1 << USART_RXEN_bp) | (1 << USART_TXEN_bp);
	
	while (!(USART0.STATUS & (1 << USART_DREIF_bp))){};	// while(sending) -> wait
	USART0.BAUD=(baud<<1);
	uart_transmit(0x00);
	while (!(USART0.STATUS & (1 << USART_DREIF_bp))){};	// while(sending) -> wait
	USART0.BAUD=baud;
	uart_transmit(0x55);
}


// baud<<1 - slow
// baud - fast