
#include "sensors.h"


void adc_init(){
	ADC0.CTRLA |= (ADC_RESSEL_10BIT_gc);      //Set resolution, we choose 8 bits

	ADC0.CTRLB |= (ADC_SAMPNUM_ACC4_gc);     //OPTIONAL: We can use multiple samples if we like, example here with 4
		                                     //More samples gives a better result but takes longer

	ADC0.CTRLC |= (ADC_REFSEL_VDDREF_gc);    //We select to use the supply voltage (VDD) as voltage reference
	ADC0.CTRLC |= (ADC_PRESC_DIV2_gc);       //ADC clock prescaler, best accuracy when run below 2MHz. (Here div2 ~1.46 MHz)

	ADC0.CTRLA |= (ADC_ENABLE_bm);           //Enable the ADC
}


uint8_t adc_read(uint8_t channel){
	ADC0.MUXPOS = channel;                   //Select input on the ADC mux
	//NOTE: We can use = here because this is the only thing the register holds. Neat!
	ADC0.INTFLAGS |= (ADC_RESRDY_bm);        //Clear the results ready flag

	ADC0.COMMAND |= (ADC_STCONV_bm);         //Start a conversion

	while(!(ADC0.INTFLAGS & ADC_RESRDY_bm)){
		                                     //Wait for the results ready flag to be set
	}
	return ADC0.RESL;                        //Return 8 bit result
}




void readSensors(struct sensorData* sd){
	turnOnSensor(PH_POWER, 'b');
	sd->temp=24;//(int)ds18b20_gettemp();	//pH has to be enabled for a long time
	//sd->temp=readTemp();
	sd->tds=readTDS(sd->temp);
	sd->turb=readTURB();
	//_delay_ms?
	_delay_ms(30000);
	sd->pH=readPH();		//needs a minute or so
}


uint16_t readTDS(float temp){   //to convert the analog values to actual values
	//turnOnSensor(TDS_POWER, 'a')		//port a
	_delay_ms(SENSOR_DELAY);
	uint16_t analogVal=adc_read(TDS_A);
	turnOffSensor(TDS_POWER, 'a');
	
	//convert to real values:
	float tds= analogVal*5/1024.0;
	float compensationCoefficient=1.0+0.02*(temp-25.0);
	float compensationVolatge=tds/compensationCoefficient;
	float tdsValue=(133.42*compensationVolatge*compensationVolatge*compensationVolatge - 255.86*compensationVolatge*compensationVolatge + 857.39*compensationVolatge)*0.5; //convert voltage value to tds value
	
	return tdsValue%256;	//guards against too high values
}

uint16_t readPH(){
	turnOnSensor(PH_POWER, 'b');		//port b
	_delay_ms(SENSOR_DELAY);
	uint16_t analogVal=adc_read(PH_A);	//needs a minute
	turnOffSensor(PH_POWER, 'b');
	
	//do something
							// 3,3V?
	float ph= analogVal * 5.0 / 1024.0; //convert the analog into millivolt
	ph = 3.5 * ph + 0.03;                //convert the millivolt into pH value
	uint16_t ret = (uint16_t)(ph*20.0);
	
	return analogVal;	
}

uint16_t readTURB(){
	turnOnSensor(TURB_POWER, 'a');		//port a
	_delay_ms(SENSOR_DELAY);
	uint16_t analogVal=adc_read(TURB_A);
	turnOffSensor(TURB_POWER, 'a');
	
	//do something
	float x = analogVal*(5 / 1024.0); // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
	float turb=(-1)*1120.4*x*x +(5742.3*x) - (4352.9)-3.77;
	uint16_t ret = (uint16_t)(turb*100);
	if (turb<0){
		return 0;
	}
	return ret;
}


void initSensors(void){
	PORTA.DIR |= (1<<TDS_POWER)|(1<<TURB_POWER);	//output, sensorPower
	PORTB.DIR |= (1<<PH_POWER)|(1<<TEMP_POWER);
	
	PORTB.DIR &= ~((1<<TDS)|(1<<TURB)|(1<<PH)|(1<<TEMP));	//input
}

void turnOnSensor(uint8_t pin, char port){
	if(port=='a' || port=='A'){
		PORTA.OUT |= (1<<pin);
	}
	else if (port=='b' || port=='B'){
		PORTB.OUT |= (1<<pin);
	}
}

void turnOffSensor(uint8_t pin, char port){
		if(port=='a' || port=='A'){
		PORTA.OUT &= ~(1<<pin);
	}
	else if (port=='b' || port=='B'){
		PORTB.OUT &= ~(1<<pin);
	}
}


/*
int16_t readTemp(){
	const uint8_t pin=0;
	//onewireInit(&PORTC_OUT, &PORTC_DIR, &PORTC_IN, (1<<pin));
	int16_t temp;
	ds18b20convert(&PORTC_OUT, &PORTC_DIR, &PORTC_IN, PORT_INT0_bm, NULL);
	_delay_ms(3000);
	int ec=ds18b20read(&PORTC_OUT, &PORTC_DIR, &PORTC_IN, (1<<pin), NULL, &temp);

	return temp;
}
*/
