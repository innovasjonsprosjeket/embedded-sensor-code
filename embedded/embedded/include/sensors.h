#ifndef SENSORS_FILE
#define SENSORS_FILE


#include <avr/io.h>
#include "ds18b20.h"


#define TDS 5		//pb5
#define TURB 4		//pb4			// remember analog channel for the mux!!!
#define PH 0		//pb0
#define TEMP 1		//pb1

#define TDS_A 5
#define TURB_A 9	//analog in
#define PH_A 11
#define TEMP_A 10

#define PH_POWER 6		//pb6
#define TEMP_POWER 7	//pb7
#define TURB_POWER 7	//pa7
#define TDS_POWER 6		//pa6

#define SENSOR_DELAY 5

struct sensorData{		// struct to store sensor data
	uint16_t temp;
	uint16_t tds;		// actual TDS value ~60
	uint16_t pH;		// pH*20
	uint16_t turb;
	};


void adc_init(void);
uint8_t adc_read(uint8_t channel);


void readSensors(struct sensorData* sd);	//assign values to all sensor data
//int16_t readTemp();
uint16_t readTDS(float temp);	//convert to actual values
uint16_t readPH();
uint16_t readTURB();

void initSensos(void);
void turnOnSensor(uint8_t pin, char port );
void turnOffSensor(uint8_t pin, char port);


#endif
