
#ifndef RADIO_FILE
#define RADIO_FILE

#include "sensors.h"
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>


#define OK "ok\r\n"
#define INVALID "invalid_param\r\n"			//all these are used in reactOnRX
#define ACCEPTED "accepted\r\n"				//common answers from the radio module
#define TX_OK "mac_tx_ok\r\n"
#define NO_FREE_CH "no_free_ch\r\n"
#define UNIN_KEYS "keys_not_init\r\n"
#define SILENT "silent\r\n"
#define BUSY "busy\r\n"
#define PAUSED "mac_paused\r\n"
#define DENIED "denied\r\n"
#define NOT_JOINED "not_joined\r\n"
#define ERROR "err\r\n"



#define BAUD_9600 ((4UL*F_CPU)/9600)
#define BAUD_57600 ((4UL*F_CPU)/57600)


void uart_init(unsigned long baud);
void uart_transmit(unsigned char data);		//one char at the time
void uart_print(const char string[]);		//used for whole strings/ commands


void radioInit(void);		                //sets dev eui, app eui and dev adr (or something)
void resetRadio(void);		                //resets radio, then radioInit()
void sendData(char sd[], uint8_t bytes);	//send char array
void dataConv(const struct sensorData* sd, char str[]);	//convert sensorData struct to char array
//void sendUInt(uint16_t num);
void sendError(char* hex);					//send char array on different port, for errors
void reactOnRX(char* hex);
void setUartRateRadio(unsigned long baud);	//not done, will set UART baud rate for radio, and wake it from sleep


#endif
